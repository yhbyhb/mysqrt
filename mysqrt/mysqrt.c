#include <stdio.h>
#include <math.h>

int main()
{
    double x = 2;
    double s_org = sqrt(x);


    int maxiter = 6;

    double s = 1.;
    int i;
    for (i = 0; i < maxiter; i++)
    {
        printf("Before iteration %d, s = %f\n", i, s);
        s = 0.5 * (s + x / s);
    }
    printf("After iteration %d, s = %f\n", i, s);

    printf("sqrt(%f) = %f\n", x, s_org);
    printf("myqsrt(%f) = %f\n", x, s);

}